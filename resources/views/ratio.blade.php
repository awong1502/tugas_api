@extends('template.app')

@section('content')

<?php
function get_CURL($url)

{
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);
curl_close($curl);

return json_decode($result, true);
}

$result = get_CURL('https://www.googleapis.com/youtube/v3/channels?part=snippet,statistics&key=AIzaSyBQFFxc5B8n1qJAuhwd7LahFZuMIin_8B0&id=UCYk4LJI0Pr6RBDWowMm-KUw');

$youtubeProfilePic = $result['items'][0]['snippet']['thumbnails']['medium']['url'];
$channelName = $result['items'][0]['snippet']['title'];
$subscriber = $result['items'][0]['statistics']['subscriberCount'];
$video = $result['items'][0]['statistics']['videoCount'];


$urlLatestVideo = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyBQFFxc5B8n1qJAuhwd7LahFZuMIin_8B0&channelId=UCYk4LJI0Pr6RBDWowMm-KUw&maxResults=1&order=date&part=snippet';
$result = get_CURL($urlLatestVideo);
$latestVideoId = $result['items'][0]['id']['videoId'];

$result_2 = get_CURL('https://www.googleapis.com/youtube/v3/channels?key=AIzaSyBQFFxc5B8n1qJAuhwd7LahFZuMIin_8B0&part=snippet,statistics&id=UC0rzsIrAxF4kCsALP6J2EsA');

$youtubeProfilePic_2 = $result_2['items'][0]['snippet']['thumbnails']['medium']['url'];
$channelName_2 = $result_2['items'][0]['snippet']['title'];
$subscriber_2 = $result_2['items'][0]['statistics']['subscriberCount'];
$video_2 = $result_2['items'][0]['statistics']['videoCount'];


$urlLatestVideo_2 = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyBQFFxc5B8n1qJAuhwd7LahFZuMIin_8B0&channelId=UC0rzsIrAxF4kCsALP6J2EsA&maxResults=1&order=date&part=snippet';
$result_2 = get_CURL($urlLatestVideo_2);
$latestVideoId_2 = $result_2['items'][0]['id']['videoId'];


?>
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="row">
            <div class="col-lg-4">
                <img src="<?= $youtubeProfilePic; ?>" width="200" class="rounded-circle img-thumbnail">
            </div>
            <div class="col-lg-8">    
                <h5><?= $channelName; ?></h5>
                <p><?= $subscriber; ?> Subscriber</p>
                <div class="g-ytsubscribe" data-channelid="UCgtqs-GaTq-XI0EGT1HfFIg" data-layout="default" data-theme="dark" data-count="default"></div>
                <p><?= $video; ?> Video Count</p>
                <div class="g-ytsubscribe" data-channelid="UCgtqs-GaTq-XI0EGT1HfFIg" data-layout="default" data-theme="dark" data-count="default"></div>
            </div>
            </div>
            <div class="row mt-3 pb-3">
            <div class="col">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= $latestVideoId ?>?rel=0" allowfullscreen></iframe>            
            </div>
            </div>
            </div>
        </div>
        <div class="col-md-5">
        <div class="row">
            <div class="col-lg-4">
                <img src="<?= $youtubeProfilePic_2; ?>" width="200" class="rounded-circle img-thumbnail">
            </div>
            <div class="col-lg-8">    
                <h5><?= $channelName_2; ?></h5>
                <p><?= $subscriber_2; ?> Subscriber</p>
                <div class="g-ytsubscribe" data-channelid="UCgtqs-GaTq-XI0EGT1HfFIg" data-layout="default" data-theme="dark" data-count="default"></div>   
                 <p><?= $video_2; ?> Video Count</p>
                <div class="g-ytsubscribe" data-channelid="UCgtqs-GaTq-XI0EGT1HfFIg" data-layout="default" data-theme="dark" data-count="default"></div>
            </div>
            </div>
            <div class="row mt-3 pb-3">
             <div class="col">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= $latestVideoId_2 ?>?rel=0" allowfullscreen></iframe>            
            </div>
            </div>
            </div>
    </div>
@endsection