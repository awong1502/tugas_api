@extends('template.app')

@section('content')
        <form action="/quiz/input" method="post">
            @csrf
            @method('post')
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                <div class="container ml-4">
                    <label for="name">Masukan Nama Anda !!</label><br>
                    <input class="" style="width: 20rem" type="text" name="nama"><br><br><br>
                </div>
                    <ul style="list-style: none">
                        <p>1. Berapakah 1 + 1 ?</p>
                        <li><input class="form-check-input" type="radio" name="soal1" value="0"> Kayaknya 2</li>
                        <li><input class="form-check-input" type="radio" name="soal1" value="0"> Sepertinya 2</li>
                        <li><input class="form-check-input" type="radio" name="soal1" value="1"> Harusnya 1</li>
                        <li><input class="form-check-input" type="radio" name="soal1" value="0"> 2</li>
                    </ul>
                    <br>
                    <ul style="list-style: none">
                        <p>2. Apakah 2 + 2 = 3 ?</p>
                        <li><input class="form-check-input" type="radio" name="soal2" value="1"> Asnan</li>
                        <li><input class="form-check-input" type="radio" name="soal2" value="0"> Asnan</li>
                        <li><input class="form-check-input" type="radio" name="soal2" value="0"> Asnan</li>
                        <li><input class="form-check-input" type="radio" name="soal2" value="0"> Asnan</li>
                    </ul>
                    <br>
                    <ul style="list-style: none">
                        <p>3. Apakah 3 hasil dari 2 + 2 ?</p>
                        <li><input class="form-check-input" type="radio" name="soal3" value="0"> Asnan</li>
                        <li><input class="form-check-input" type="radio" name="soal3" value="1"> Asnan</li>
                        <li><input class="form-check-input" type="radio" name="soal3" value="0"> Asnan</li>
                        <li><input class="form-check-input" type="radio" name="soal3" value="0"> Asnan</li>
                    </ul>
                    <br>
                    <ul style="list-style: none">
                        <p>4. Apakah 1 merupakan hasil dari 1 + 1 ?</p>
                        <li><input class="form-check-input" type="radio" name="soal4" value="1"> Asnan</li>
                        <li><input class="form-check-input" type="radio" name="soal4" value="0"> Asnan</li>
                        <li><input class="form-check-input" type="radio" name="soal4" value="0"> Asnan</li>
                        <li><input class="form-check-input" type="radio" name="soal4" value="0"> Asnan</li>
                    </ul>
                    <button style="margin-left: 2.5rem" class="btn btn-primary" type="submit">Submit Jawaban</button>
                </label>
            </div>
        </form>

@endsection