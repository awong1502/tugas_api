<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PortofolioController;
use App\Http\Controllers\QuizController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('/main', [PortofolioController::class, 'View']);

Route::get('/input', [PortofolioController::class, 'portofolioCreateView']);

Route::post('/input', [PortofolioController::class, 'portofolioCreate']);

Route::get('/edit/{id}', [PortofolioController::class, 'portofolioEditView']);

Route::get('/edit/{id}', [PortofolioController::class, 'portofolioEditViewData']);

Route::put('/edit/{id}', [PortofolioController::class, 'portofolioEditViewDataUpdate']);

Route::get('/delete/{id}', [PortofolioController::class, 'delete']);

Route::get('/ratio', function () {
    return view('ratio');
});

Route::get('/quiz', [QuizController::class, 'quizView']);

Route::post('/quiz/input', [QuizController::class, 'quizInput']);

Route::get('/nilai', [QuizController::class, 'nilaiQuiz']);

Route::get('/cek', [QuizController::class, 'nilaiQuizView']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
