<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\quiz;
use Illuminate\Support\Facades\Auth;

class QuizController extends Controller
{
    public function quizView() {
        $tes = 1 ;
        return view('quiz' , ['tes'=>$tes]);
    }

    public function quizInput(Request $Request) {
        $quiz = new quiz();
        $soal_1 = $Request->soal1;
        $soal_2 = $Request->soal2;
        $soal_3 = $Request->soal3;
        $soal_4 = $Request->soal4;
        $plus = ( $soal_1 + $soal_2 + $soal_3 + $soal_4 );
        $result = ( $plus/4 ) *100;

        $quiz->user_id = Auth::user()->id;
        $quiz->nama = $Request->nama;
        $quiz->nilai = $result;
        $quiz->save();
        return redirect()->back();
    }

    public function nilaiQuiz(){
        $data = quiz::all();

        return view('nilai', ['data' => $data]);
    }
}