<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\portofolio;
use App\Models\quiz;
use Illuminate\Support\Facades\Auth;

class PortofolioController extends Controller
{
    public function View(){
        $data = portofolio::all();

        return view('main', ['profile' => $data]);
    }

    public function portofolioCreateView(){
        return view('compare');
    }

    public function portofolioCreate(Request $Request){
        $product = new portofolio();
        $product->user_id = Auth::user()->id;
        $product->stambuk = $Request->stambuk;
        $product->kelas = $Request->kelas;
        $product->judul_tugas = $Request->judul;
        $product->save();
        return redirect()->back();
    }

    public function portofolioEditView($id){
        return view('edit');
    }

    public function portofolioEditViewData($id){
        $dataEdit = portofolio::where('id',$id)->first();
        
        return view('edit', ['profile' => $dataEdit]);
    }

    public function portofolioEditViewDataUpdate(Request $Request , $id){
        $updateProduct = portofolio::where('id',$id)->first();
        $updateProduct->stambuk = $Request->stambuk;
        $updateProduct->kelas = $Request->kelas;
        $updateProduct->judul_tugas = $Request->judul_tugas;
        $updateProduct->update();
        return redirect('/main');
    }

    public function delete($id){
        $data = portofolio::where('id', $id)->first();

        $data->delete();
        return redirect()->back();
    }
}
