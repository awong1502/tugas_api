@extends('template.app')

@section('content')
    <div class="form-group">
        <form action="/input" method="post">
            @csrf
            @method('post')
        <label for="">Stambuk</label>
        <input type="text" name="stambuk" id="" class="form-control" placeholder="" aria-describedby="helpId">
        <small id="helpId" class="text-muted">Input NIM Anda</small><br><br>
        <label for="">Kelas</label>
        <input type="text" name="kelas" id="" class="form-control" placeholder="" aria-describedby="helpId">
        <small id="helpId" class="text-muted">Input Kelas</small><br><br>
        <label for="">Judul Tugas</label>
        <input type="text" name="judul" id="" class="form-control" placeholder="" aria-describedby="helpId">
        <small id="helpId" class="text-muted">Input Judul Tugas</small><br>
        <button class="btn btn-primary" type="submit">Submit</button>
    </form>
    </div>
@endsection