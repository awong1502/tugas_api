@extends('template.app')

@section('content')
<div class="container">
    <div class="table-1">
    <table class="table">
        @foreach ($data as $d)
            
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Nilai</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $d->nama }}</td>
                    <td>{{ $d->nilai }}</td>
                </tr>
            </tbody>
        
        @endforeach
    </div>
    </table>

    <div class="table-2">
    <table>
            <tr>
                <th>
                    ANJAS
                </th>
                <th>
                    
                </th>
                <th>
                    <input type="email" class="form-control" name="" id="" aria-describedby="emailHelpId" placeholder="">
                </th>
            </tr>
            <tr>
            </tr>
            <tr>
                <td colspan="2">
                    <a href="" class="btn btn-primary" >Submit</a>
                </td>
            </tr>
        </table>
    </div>
    </div>
@endsection