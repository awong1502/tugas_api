@extends('template.app')

@section('content')
<div class="container col-md-4">      
    <form method="post" action="/edit/{{ $profile->id }}">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="exampleInputEmail1">Stambuk</label>
            <input type="text" class="form-control" name="stambuk" value="{{ $profile->stambuk }}">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Kelas</label>
            <input type="text" class="form-control" name="kelas" value="{{ $profile->kelas }}">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Judul Tugas</label>
            <input type="text" class="form-control" name="judul_tugas" value="{{ $profile->judul_tugas }}">
            </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection