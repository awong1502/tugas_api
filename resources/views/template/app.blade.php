
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <title>
    SIMPLE CRUD
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  

  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/paper-dashboard.css?v=2.0.1') }}" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <div class="logo">
        <a href="https://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="../assets/img/logo-small.png">
          </div>
        </a>
        <a href="#" class="simple-text logo-normal" style="font-size: 12px">
          {{ Auth::user()->name }}
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="{{ request()->is('main') ? 'active' : '' }}">
            <a href="/main">
              <i class="nc-icon nc-bank"></i>
              <p>Profil</p>
            </a>
          </li>
          <li class="{{ request()->is('input') ? 'active' : '' }}">
            <a href="/input">
              <i class="nc-icon nc-diamond"></i>
              <p>Tambah Data</p>
            </a>
          </li>
          <li class="{{ request()->is('ratio') ? 'active' : '' }}">
            <a href="/ratio">
              <i class="nc-icon nc-diamond"></i>
              <p>Youtube Favorit</p>
            </a>
          </li>
          <li>
          <li class="{{ request()->is('quiz') ? 'active' : '' }}">
            <a href="/quiz">
              <i class="nc-icon nc-diamond"></i>
              <p>Soal Quiz</p>
            </a>
          </li>
          <li>
          <li class="{{ request()->is('nilai') ? 'active' : '' }}">
            <a href="/nilai">
              <i class="nc-icon nc-diamond"></i>
              <p>Niai Quiz</p>
            </a>
          </li>
          <li>
            <a href="/home">
              <i class=""></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            @if (request()->is('main'))   
              <a class="navbar-brand" href="javascript:;">Main Profile</a>
            @elseif (request()->is('input'))
              <a class="navbar-brand" href="javascript:;">Input Data</a>
            @elseif (request()->is('ratio'))
              <a class="navbar-brand" href="javascript:;">Perbandingan Youtube Deddy Corbuzier & Raditya Dika</a>
            @elseif (request()->is('quiz'))
              <a class="navbar-brand" href="javascript:;">Soal Quiz</a>
            @elseif (request()->is('nilai'))
              <a class="navbar-brand" href="javascript:;">Nilai Quiz</a>
            @endif
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
          @yield('content')
      </div>
        
      </div>
    </div>
  </div>
</body>

</html>
