@extends('template.app')

@section('content')
    <table class="table table-borderless">  
        @foreach ($profile as $pr)
            <thead>
            </thead>
            <tbody>
                @if (Auth::user()->id == 3)
                    <tr>
                        <th scope="row">Nama</th>
                        <th>{{ $pr->user['name'] }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Email</th>
                        <th>{{ $pr->user['email'] }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Stambuk</th>
                        <th>{{ $pr->stambuk }}</th>
                        <th><a href="/edit/{{ $pr->id }}" class="btn btn-info">EDIT</a></th>
                        <th><a href="/delete/{{ $pr->id }}" class="btn btn-danger">DELETE</a></th>
                    </tr>
                    <tr>
                        <th scope="row">Kelas</th>
                        <th>{{ $pr->kelas }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Judul Tugas</th>
                        <th>{{ $pr->judul_tugas }}</th>
                    </tr>
                    <tr>
                        <td colspan="4"><hr></td>
                    </tr>
                    </tbody>
                @else
                    <tr>
                        <th scope="row">Nama</th>
                        <th>{{ $pr->user['name'] }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Email</th>
                        <th>{{ $pr->user['email'] }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Stambuk</th>
                        <th>{{ $pr->stambuk }}</th>
                        @if (Auth::user()->id == $pr->user['id'])
                            <th><a href="/edit/{{ $pr->id }}" class="btn btn-info">EDIT</a></th>
                            <th><a href="/delete/{{ $pr->id }}" class="btn btn-danger">DELETE</a></th>
                        @endif
                            </tr>
                            <tr>
                                <th scope="row">Kelas</th>
                                <th>{{ $pr->kelas }}</th>
                            </tr>
                            <tr>
                                <th scope="row">Judul Tugas</th>
                                <th>{{ $pr->judul_tugas }}</th>
                            </tr>
                            <tr>
                                <td colspan="4"><hr></td>
                            </tr>
                    </tbody>
                @endif
        @endforeach
    </table>
    
    
@endsection